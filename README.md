# Message API - internship test task [![pipeline status](https://gitlab.com/innopolis-university-assignments/test_internship2020/badges/master/pipeline.svg)](https://gitlab.com/innopolis-university-assignments/test_internship2020/-/commits/master)
This microservice provides API for message transactions. 

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Installation](#installation)
- [Running](#running)
- [Testing](#testing)
- [Authentication](#authentication)
- [Endpoints that require Authentication](#endpoints-that-require-authentication)
    - [Create a new message](#create-a-new-message)
    - [Get the list of all messages](#get-the-list-of-all-messages)
    - [Retrieve one message](#retrieve-one-message)
    - [Update text of the message](#update-text-of-the-message)
    - [Delete the message](#delete-the-message)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Information about DevOps test task
Link to website: danil-usmanov-test.ga

### What was done:
* CI/CD: build, test, deploy stages;
* Set up docker-compose on server
* Get domain name
> it's really hard to get a free domain
*  ~~SSL~~ - As I started do this task on Saturday I didn't do it(

>  I would like to add that it was a bad idea to take my previous project, it would be better if I have got a ready project. Just because I didn't configure the project to deploy, didn't set up nginx/apache







## Technology stack
* Python3
* Django
* Django Rest Framework
* Docker

## Docs

You can go to `/docs/` and view interactive doc of enpoints.


# Installation
    git clone https://gitlab.com/innopolis-university-assignments/test_internship2020.git

# Running
    docker-compose up --build

# Testing
    docker-compose run web python /code/message_api/manage.py test


# Authentication

Authentication uses a simple method **_username_** and **_password_**.

## Users
You can create a new user in Django admin panel.

### Created users:

```
login: admin
password: 12345678
acces level: admin
```
```
login: user1
password: W3p46OlQ
acces level: user
```
```
login: user2
password: Jg>5>t7?`
acces level: user
```


# Endpoints that require Authentication


## Create a new message

---

Used to create a message for a registered User.

**URL** : `/messages/`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

```json
{
    "text": "[message text]",
    "sender": "[user id]",
    "receiver": "[receiver id]"
}
```

**Data example**

```json
{
    "text": "Nice example of message!",
    "sender": 1,
    "receiver": 3
}
```

## Success Response

**Code** : `201 Created`

**Content example**

```json
{
    "id": 14,
    "text": "Nice example of message!",
    "creation_date": "2020-04-23T19:38:49.436501+03:00",
    "update_date": "2020-04-23T19:38:49.436552+03:00",
    "sender": 1,
    "receiver": 3
}
```

## Error Response

**Condition** : If  'text' field is empty.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "text": [
        "This field may not be blank."
    ]
}
```

**Condition** : If  'sender' equal to 'receiver'.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "non_field_errors": [
        "The sender can not send a message to himself!"
    ]
}
```

**Condition** : If  'sender' not equal to registered User.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "non_field_errors": [
        "The sender should be registered user!"
    ]
}
```

**Condition** : If  'sender' or 'receiver doesn't exists.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "receiver": [
        "Invalid pk \"5\" - object does not exist."
    ]
}
```



## Get the list of all messages

---

Used to get all messages where registered User participates.

**URL** : `/messages/`

**Method** : `GET`

**Auth required** : YES

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    [
    {
        "id": 1,
        "text": "Message1-updated5",
        "creation_date": "2020-04-20T21:55:02.205165+03:00",
        "update_date": "2020-04-22T15:18:05.700582+03:00",
        "sender": 2,
        "receiver": 2
    },
    {
        ...
    }
    ]
}
```

## Error Response

**Condition** : If not registered user makes the request.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "detail": "Authentication credentials were not provided."
}
```



## Retrieve one message

---

Used to retrieve a message for a registered User.

**URL** : `/messages/pk/`

**Method** : `GET`

**Auth required** : YES

**Data constraints**

```json
{
    "id": "[message id]",
    "text": "[message text]",
    "creation_date": "[creation date and time]",
    "update_date": "[updated date and time]",
    "sender": "[user id]",
    "receiver": "[receiver id]"
}
```

**Data example**

```json
{
    "id": 1,
    "text": "Nice example of message!",
    "creation_date": "2020-04-20T21:55:02.205165+03:00",
    "update_date": "2020-04-22T15:18:05.700582+03:00",
    "sender": 1,
    "receiver": 3
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "id": 1,
    "text": "Message1-updated5",
    "creation_date": "2020-04-20T21:55:02.205165+03:00",
    "update_date": "2020-04-22T15:18:05.700582+03:00",
    "sender": 2,
    "receiver": 2
}
```

## Error Response

**Condition** : If not registered user makes the request.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "detail": "Authentication credentials were not provided."
}
```

**Condition** : If user tries to get message where he is not sender or receiver.

**Code** : `404 Not Found`

**Content** :

```json
{
    "detail": "Authentication credentials were not provided."
}
```


## Update text of the message

---

Used to update text of the message for a registered User.

**URL** : `/messages/pk/`

**Method** : `PATCH`

**Auth required** : YES

**Data constraints**

```json
{
    "text": "[new message text]",
}
```

**Data example**

```json
{
    "text": "New nice example of message!"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "id": 1,
    "text": "New nice example of message!",
    "creation_date": "2020-04-20T21:55:02.205165+03:00",
    "update_date": "2020-04-23T20:10:14.355994+03:00",
    "sender": 2,
    "receiver": 1
}
```

## Error Response

**Condition** : If not registered user makes the request.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "detail": "Authentication credentials were not provided."
}
```

**Condition** : If registered user tries update message where he is receiver.

**Code** : `400 Bad Request`

**Content** :

```json
{
    "non_field_errors": [
        "The registered user is not sender!"
    ]
}
```

**Condition** : If registered user tries update message to another user.

**Code** : `400 Bad Request`

**Content** :

```json
{
    "non_field_errors": [
        "The registered user is not sender!"
    ]
}
```



## Delete the message

---

Used to delete a message for a registered User.

**URL** : `/messages/pk/`

**Method** : `DELETE`

**Auth required** : YES

**Data constraints**

```json
{
    "id": "[message id]",
    "text": "[message text]",
    "creation_date": "[creation date and time]",
    "update_date": "[updated date and time]",
    "sender": "[user id]",
    "receiver": "[receiver id]"
}
```

**Data example**

```json
{
    "id": 1,
    "text": "Nice example of message!",
    "creation_date": "2020-04-20T21:55:02.205165+03:00",
    "update_date": "2020-04-22T15:18:05.700582+03:00",
    "sender": 1,
    "receiver": 3
}
```

## Success Response

**Code** : `204 No Content`

**Content example**

```
```

## Error Response

**Condition** : If not registered user makes the request.

**Code** : `401 Unauthorized`

**Content** :

```json
{
    "detail": "Authentication credentials were not provided."
}
```

**Condition** : If user tries to delete message of another user.

**Code** : `404 Not Found`

**Content** :

```json
{
     "detail": "Not found."
}
```

**Condition** : If user tries to delete message where he is receiver.

**Code** : `400 Bad Request`

**Content** :

```json
{
     "non_field_errors": [
        "The registered user is not sender!"
    ]
}
```
