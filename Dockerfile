FROM python:3

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install requirements
COPY requirements.txt .
RUN pip install -r requirements.txt

# Copy project
WORKDIR /code
COPY . /code/

# Server
EXPOSE 80
STOPSIGNAL SIGINT
# ENTRYPOINT ["python", "manage.py"]
