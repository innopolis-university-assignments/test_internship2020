from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from rest_framework.documentation import include_docs_urls

from .views import MessageViewSet

router = routers.DefaultRouter()
router.register(r"messages", MessageViewSet)

# Title and description of the document page
API_TITLE = "Message API"
API_DESCRIPTION = "Message API: Follow CRUD principe. Allow create/get/update/delete a message."

urlpatterns = [
    path("", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("admin/", admin.site.urls),
    path("docs/", include_docs_urls(title=API_TITLE, description=API_DESCRIPTION)),  # The page with docs
]
