from rest_framework.response import Response
from rest_framework import viewsets, mixins, status
from rest_framework import permissions
from .models import Message
from .serializers import MessageSerializer
from django.db.models import Q


class MessageViewSet(viewsets.GenericViewSet,
                     mixins.ListModelMixin,  # Allow get list
                     mixins.CreateModelMixin,  # Allow create an object
                     mixins.RetrieveModelMixin,  # Allow get an object
                     mixins.DestroyModelMixin,  # Allow delete an object
                     mixins.UpdateModelMixin):  # Allow update an object
    """
    API endpoint that allows messages to be viewed or edited.
    """
    serializer_class = MessageSerializer
    permission_classes = [permissions.IsAuthenticated]
    queryset = Message.objects.all()
    http_method_names = ["get", "post", "head", "patch", "delete"]  # Allow only these methods

    def get_queryset(self):
        """Return object for current authenticated user only"""
        return self.queryset.filter(Q(sender=self.request.user) | Q(receiver=self.request.user))

    def partial_update(self, request, *args, **kwargs):
        """Deny updating if registered user is not sender"""
        instance = self.queryset.get(pk=kwargs.get("pk"))
        context = {
                    "request_sender" : request.user.id,
                    "request_method" : request.method
        }
        serializer = self.serializer_class(instance, data=request.data, partial=True, context=context)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
            
    def destroy(self, request, *args, **kwargs):
        """Deny deleting if registered user is not sender"""
        instance = self.get_object()
        context = {
                    "request_sender" : request.user.id,
                    "request_method" : request.method
        }
        instance_validation = self.queryset.get(pk=kwargs.get("pk"))
        serializer = self.serializer_class(instance_validation, data=request.data, partial=True, context=context)
        serializer.is_valid(raise_exception=True)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
