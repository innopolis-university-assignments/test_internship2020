from rest_framework import serializers

from .models import Message


class MessageSerializer(serializers.ModelSerializer):
    # The main point is to allow fields sender and receiver be creatable but not updatable
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)  # Create an instance of class ModelSerializer and add all data
        if self.instance is not None:  # If the instance exists in DB(not create action) make it read_only
            self.fields.get("sender").read_only = True
            self.fields.get("receiver").read_only = True

    class Meta:
        model = Message
        fields = "__all__"  # We show all fields of model
        read_only_fields = ("id", "creation_date", "update_date")  # These fields can"t be custom change

    def validate(self, data):
        if self.context.get("request_method") == "PATCH":
            # If registered user tries to update message where he is not sender
            if self.context.get("request_sender") != Message.objects.get(id=self.instance.id).sender.id:
                raise serializers.ValidationError("The registered user is not sender!")

        if self.context.get("request_method") == "DELETE":
            # If registered user tries to delete message where he is not sender
            if self.context.get("request_sender") != Message.objects.get(id=self.instance.id).sender.id:
                raise serializers.ValidationError("The registered user is not sender!")

        # When we update/delete instance this field won"t be included in data;
        if "sender" in data:  # If a sender is included then receiver too, so one checking is enough.
            sender = data["sender"]
            receiver = data["receiver"]
            user = self.context["request"].user

            if sender != user:  # If the logged-in user tries to send a message from other user
                raise serializers.ValidationError("The sender should be registered user!")

            if sender == receiver and sender == user:  # If the logged-in user tries to send a message to himself
                raise serializers.ValidationError("The sender can not send a message to himself!")

        return data
