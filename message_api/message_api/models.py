from django.contrib.auth.models import User
from django.db import models


class Message(models.Model):
    # Sender and receiver should exist, in another case, all instances with an undefined user will be deleted
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="senders")
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name="receivers")
    text = models.TextField()

    # Get the current time when you first add an instance in the DB
    creation_date = models.DateTimeField(auto_now_add=True)
    # After each update of an instance in the DB previous value is replaced by the current time
    update_date = models.DateTimeField(auto_now=True)
