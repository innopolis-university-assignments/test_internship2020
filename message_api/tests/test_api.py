from django.contrib.auth.models import User
from rest_framework.test import APITestCase

"""Set of unit tests"""


class MessageApiTest(APITestCase):

    def setUp(self):
        """Create users"""
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"

        self.user = User.objects.create_user(
            self.username, self.email, self.password
        )

        self.username2 = "lanister"
        self.email2 = "lanister@snow.com"
        self.password2 = "lanistersalwayspay"

        self.user2 = User.objects.create_user(
            self.username2, self.email2, self.password2
        )

        self.username3 = "targarien"
        self.email3 = "targarien@snow.com"
        self.password3 = "bloodfire"

        self.user3 = User.objects.create_user(
            self.username3, self.email3, self.password3
        )

    """Autorization tests"""

    def test_authorized_authentication(self):
        self.client.login(username=self.username, password=self.password)
        response = self.client.get("/messages/")

        self.assertEqual(200, response.status_code)

    def test_unauthorized_authentication(self):
        self.client.login(username="fake", password="fake_password")
        response = self.client.get("/messages/")

        self.assertEqual(401, response.status_code)

    """Creation tests"""

    def test_create_message(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 2
        }
        response = self.client.post("/messages/", data)
        json = response.json()

        self.assertEqual(201, response.status_code)
        self.assertEqual(json["text"], data["text"])
        self.assertEqual(json["sender"], data["sender"])
        self.assertEqual(json["receiver"], data["receiver"])

    def test_create_empty_text(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "",
            "sender": 1,
            "receiver": 2
        }
        response = self.client.post("/messages/", data)
        json = response.json()

        self.assertEqual(400, response.status_code)
        self.assertEqual(json["text"][0], "This field may not be blank.")

    def test_create_mesage_to_myself(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 1
        }
        response = self.client.post("/messages/", data)
        json = response.json()

        self.assertEqual(400, response.status_code)
        self.assertEqual(json["non_field_errors"][0], "The sender can not send a message to himself!")


    def test_create_message_from_another_user(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 2,
            "receiver": 1
        }
        response = self.client.post("/messages/", data)
        json = response.json()

        self.assertEqual(400, response.status_code)
        self.assertEqual(json["non_field_errors"][0], "The sender should be registered user!")

    def test_create_message_from_another_to_himself(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 2,
            "receiver": 1
        }
        response = self.client.post("/messages/", data)
        json = response.json()

        self.assertEqual(400, response.status_code)
        self.assertEqual(json["non_field_errors"][0], "The sender should be registered user!")

    def test_create_message_to_undefine_user(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 5
        }
        response = self.client.post("/messages/", data)
        json = response.json()

        self.assertEqual(400, response.status_code)
        self.assertEqual(json["receiver"][0], "Invalid pk \"{}\" - object does not exist.".format(data["receiver"]))

    def test_create_message_from_undefine_user(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 5,
            "receiver": 1
        }
        response = self.client.post("/messages/", data)
        json = response.json()

        self.assertEqual(400, response.status_code)
        self.assertEqual(json["sender"][0], "Invalid pk \"{}\" - object does not exist.".format(data["sender"]))

    def test_create_message_invalid_id(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "id": 999,
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 2
        }
        response = self.client.post("/messages/", data)
        json = response.json()

        self.assertEqual(201, response.status_code)
        self.assertEqual(json["id"], 1)  # As each test starts independently, this fields gets id=1

    """Retriving tests"""

    def test_get_list_for_registered_user(self):
        self.client.login(username=self.username, password=self.password)
        response = self.client.get("/messages/")
        self.assertEqual(200, response.status_code)
        self.assertIsInstance(response.json(), list)

    def test_get_message(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 2
        }
        self.client.post("/messages/", data)
        response = self.client.get("/messages/1/")  # As each test starts independently, this fields gets id=1
        json = response.json()
        response_text = json["text"]
        response_sender = json["sender"]
        response_receiver = json["receiver"]

        self.assertEqual(200, response.status_code)
        self.assertEqual(response_text, data["text"])
        self.assertEqual(response_sender, data["sender"])
        self.assertEqual(response_receiver, data["receiver"])


    def test_get_message_another_user(self):
        self.client.login(username=self.username2, password=self.password2)
        data = {
            "text": "Nice example of message!",
            "sender": 2,
            "receiver": 3
        }
        self.client.post("/messages/", data)

        self.client.login(username=self.username, password=self.password)
        response = self.client.get("/messages/1/")  # As each test starts independently, this fields gets id=1
        self.assertEqual(404, response.status_code)

    """Updating tests"""

    def test_update_message(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 2
        }
        new_data = {
            "text": "New nice example of message!"
        }

        self.client.post("/messages/", data)

        response = self.client.patch("/messages/1/",
                                     new_data)  # As each test starts independently, this fields gets id=1
        json = response.json()
        response_text = json["text"]

        self.assertEqual(200, response.status_code)
        self.assertEqual(new_data["text"], response_text)

    def test_update_id(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 2
        }
        new_data = {
            "id": 10,
            "text": "New nice example of message!"
        }

        self.client.post("/messages/", data)

        response = self.client.patch("/messages/1/",
                                     new_data)  # As each test starts independently, this fields gets id=1
        json = response.json()
        response_text = json["text"]
        response_id = json["id"]

        self.assertEqual(200, response.status_code)
        self.assertEqual(new_data["text"], response_text)
        self.assertEqual(response_id, 1)
        assert new_data["text"] == response_text  # As each test starts independently, this fields gets id=1

    def test_update_sender(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 2
        }
        new_data = {
            "text": "New nice example of message!",
            "sender": 3
        }

        self.client.post("/messages/", data)

        response = self.client.patch("/messages/1/",
                                     new_data)  # As each test starts independently, this fields gets id=1
        json = response.json()
        response_sender = json["sender"]

        self.assertEqual(200, response.status_code)
        self.assertEqual(data["sender"], response_sender)

    def test_update_receiver(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 2
        }
        new_data = {
            "text": "New nice example of message!",
            "receiver": 3
        }

        self.client.post("/messages/", data)

        response = self.client.patch("/messages/1/",
                                     new_data)  # As each test starts independently, this fields gets id=1
        json = response.json()
        response_receiver = json["receiver"]

        self.assertEqual(200, response.status_code)
        self.assertEqual(data["receiver"], response_receiver)

    def test_update_another_user(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 2
        }
        new_data = {
            "text": "New nice example of message!"
        }

        self.client.post("/messages/", data)

        self.client.login(username=self.username2, password=self.password2)
        response = self.client.patch("/messages/1/",
                                     new_data)  # As each test starts independently, this fields gets id=1

        self.assertEqual(400, response.status_code)

    """Deletion tests"""

    def test_delete(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 2
        }
        self.client.post("/messages/", data)

        response = self.client.delete("/messages/1/")  # As each test starts independently, this fields gets id=1
        self.assertEqual(204, response.status_code)

    def test_delete_undefine_message(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 2
        }
        self.client.post("/messages/", data)

        response = self.client.delete("/messages/10/")
        self.assertEqual(404, response.status_code)

    def test_delete_another_user_message(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 3
        }
        new_data = {
            "text": "New nice example of message!"
        }

        self.client.post("/messages/", data)

        self.client.login(username=self.username2, password=self.password2)
        response = self.client.delete("/messages/1/")  # As each test starts independently, this fields gets id=1

        self.assertEqual(404, response.status_code)

    def test_delete_from_another_user_message(self):
        self.client.login(username=self.username, password=self.password)
        data = {
            "text": "Nice example of message!",
            "sender": 1,
            "receiver": 2
        }
        new_data = {
            "text": "New nice example of message!"
        }

        self.client.post("/messages/", data)

        self.client.login(username=self.username2, password=self.password2)
        response = self.client.delete("/messages/1/")  # As each test starts independently, this fields gets id=1

        self.assertEqual(400, response.status_code)
